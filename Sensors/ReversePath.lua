local sensorInfo = {
	name = "Form_NGroups",
	desc = "Split in N groups",
	author = "Dario Lanza",
	date = "2017-11-04",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- instant, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description Take passed positons, do some transformations over them and return the result back

local function ReversePath(myPath)
	local iterations = #myPath
	local reversedPath = {}
	for index,localnode in pairs(myPath)do
		reversedPath[iterations] = localnode
		iterations = iterations - 1
	end
	reversedPath[1] = myPath[#myPath]
	return reversedPath
end

return function (AtlasGroup)
	
	
		--	myTable = { PathIndex = 1 , UnitStucked = 0 ,StuckedTime = 0 , OrderGiven = false , path = mpath }
	local AtlasID = AtlasGroup[1]
	Spring.Echo ("AtlasID " .. AtlasID)
	local myTable = bb[AtlasID]
	myTable.PathIndex = 0
	
	local path =  myTable.path
	local reversedPath = ReversePath(path)
	myTable.path = reversedPath
	bb[AtlasID] = myTable
	return nil
	
	
end





