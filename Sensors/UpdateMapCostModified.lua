local sensorInfo = {
	name = "Form_NGroups",
	desc = "Split in N groups",
	author = "Dario Lanza",
	date = "2017-11-04",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- instant, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description Take passed positons, do some transformations over them and return the result back

local function ValidIndex(index,Map)
	if ( index < 1 or index > (Map.granularity_x -1)* (Map.granularity_z-1))then
		return false
	else
		return true
	end	
	

end

local function NewNode (index,cost,parent,pos_x,pos_y, pos_z)

	local node = { index = index,cost = cost,parent = parent , x = pos_x, y=pos_y, z = pos_z}


return node
end

local function GetIndex (pos,tempMap)
		--local Max_x = Game.mapSizeX/ Map.granularity_x
	
	local tmpx = Game.mapSizeX/tempMap.granularity_x
	local tmpz = Game.mapSizeZ/tempMap.granularity_z
	local x = pos.x/tmpx
	local z = pos.z/tmpz
	local myfileName = "test.txt"
	myfile = io.open(myfileName, "w")
	local i = math.floor(x)
	if(i - x >= 0.5)then
		i = math.ceil(x)	
	end
	
	local j = math.ceil(z)
	if(j - z >= 0.5)then
		j = math.ceil(z)	
	end
	myfile:write (" x " .. x ..  " z " .. z .. " tmpx " ..tmpx .. " tmpz " .. tmpz .. " pos.x ".. pos.x .. "pos.z" .. pos.z   )
	myfile:write (" i ".. i .. " j " .. j )
	--index = j + i  * Map.granularity_x 
	
	-- WORKING FORMULA (provided the grid is set-up as before)local index = (j)+ (i ) * Map.granularity_z 
	local index = j + i  * tempMap.granularity_z 
	--io.close
	return index
	
	
	


end

local function GetTrueCoordinates(index,tempMap)
	local node = Grid[index]
	if(node == nil)then
	Spring.Echo("error is here")
	end
--	myfile:write("index is " .. node.index .. " \n")
	return  Vec3( math.ceil (node.x +  tempMap.granularity_x / 2),math.ceil(node.y) , math.ceil (node.z + tempMap.granularity_z/2  ))
	
	
end


local function UpdateMap ()


end

return function (pair,Map,radius,CostIncrease)
		--local myfileName = "UpdateMapCost.txt"
	--myfile = io.open(myfileName, "w")


		--	myTable = { PathIndex = 1 , UnitStucked = 0 ,StuckedTime = 0 , OrderGiven = false , path = mpath }
	local AtlasID = pair.AtlasID
	if(Spring.ValidUnitID(atlasID))then
		return nil
	end	
	
	--Spring.Echo ("AtlasID " .. AtlasID)
	local mypathTable = bb.pathsTable
	local myTable = mypathTable[AtlasID]
	local pos =  myTable.path[myTable.PathIndex]

	local tempMap = bb.Map 
	Grid = tempMap.Grid
	local gridIndex = GetIndex(pos,tempMap)  --Sensors.nota_dal_myfourthproject.myPosition(listOfUnits[i])
	--local tmpNumber = (Game.mapSizeZ/tempMap.granularity_z)
	--local range = math.floor (MaxRange/ tmpNumber)
	local firstNodeIndex =  gridIndex - radius - radius* tempMap.granularity_z
	local iterations =2*radius 
	--myfile:write("central square is node with index " .. gridIndex .. " range is " .. radius .. "\n")
	for i = 0,iterations + 1 do
		for j = 0,iterations  do 
			local tempIndex = firstNodeIndex + j + i *  tempMap.granularity_z
			if(ValidIndex( tempIndex,tempMap) == true)then
			
				--Spring.Echo("tempIndex " .. tempIndex)
				local node = Grid[tempIndex]
				--[[local nodePos = GetTrueCoordinates(node.index,tempMap)
				
				local dist = nodePos:Distance(pos) 
				local distInGridUnits = math:ceil (dist / tempMap.granularity_x)
				if(distInGridUnits < 1)then
					dist = 1
				end	
				local tmpCost = CostIncrease/dist
				local finalCost = node.cost + tmpCost--]]
				local UpdatedNode = NewNode ( node.index, node.cost + CostIncrease  ,node.parent , node.x, node.y , node.z )
				--myfile:write ("node index " .. node.index .. " is now dangerous with cost " .. UpdatedNode.cost .. "\n")
				Grid[tempIndex] = UpdatedNode
			end
		end
	end
	--myfile:write("END\n")
	tempMap.Grid = Grid
	bb.Map = tempMap
	--[[if(tempMap ~= nil )then
		bb.Map = tempMap
		return nil
	end--]]
end





