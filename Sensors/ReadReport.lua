local sensorInfo = {
	name = "Form_NGroups",
	desc = "Split in N groups",
	author = "Dario Lanza",
	date = "2017-11-04",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description Take passed positons, do some transformations over them and return the result back





-- @description return filtered list of listOfUnits
-- @argument numberOfGroups number 
-- @argument listOfUnits [array of unitID] unfiltered list 

-- @return groupArrays [table] filtered list

return function( report )
		--[[  
		TaskFields:
			Task.SavedUnits
			Task.DangerousUnits
			Task.DeadUnits
			Task.UnitsToRescue 
			report.GroupDead
		--]]
		local pairedUnits =  report.pairedUnits
		local AtlasID = pairedUnits.AtlasID
		local UnitToRescueID = pairedUnits.UnitToRescueID
		
		
		
		
		if( bb[AtlasID] ~= nil and bb[UnitToRescueID] ~= nil )then
			bb[AtlasID] = nil
			bb[UnitToRescueID] = nil			
		end
		
	return nil
end
