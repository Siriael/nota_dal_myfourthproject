local sensorInfo = {
	name = "Form_NGroups",
	desc = "Split in N groups",
	author = "Dario Lanza",
	date = "2017-11-04",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- instant, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description Take passed positons, do some transformations over them and return the result back

local function ValidIndex(index,Map)
	if ( index < 1 or index > (Map.granularity_x -1)* (Map.granularity_z-1))then
		return false
	else
		return true
	end	
	

end

local function NewNode (index,cost,parent,pos_x,pos_y, pos_z)

	local node = { index = index,cost = cost,parent = parent , x = pos_x, y=pos_y, z = pos_z}


return node
end

local function GetIndex (pos,Map)
	local tmpx = Game.mapSizeX/Map.granularity_x
	local tmpz = Game.mapSizeZ/Map.granularity_z
	local x = pos.x/tmpx
	local z = pos.z/tmpz

	local i = math.ceil(x)
	local j = math.ceil(z)
		local index = (j)+ (i - 1) * Map.granularity_z 
	
	return index


end


local function UpdateMap(enemyUnitID,MaxRange,CostIncrease,Map)

	local x,y,z = Spring.GetUnitPosition(enemyUnitID)
	local pos =  Vec3(x,y,z)
	local gridIndex = GetIndex(pos,Map)  
	local tmpNumber = (Game.mapSizeZ/Map.granularity_z)
	local range = math.floor (MaxRange/ tmpNumber)
	local firstNodeIndex =  gridIndex - range - range* Map.granularity_x
	local iterations =2*range 
	myfile:write("central square is node with index " .. gridIndex .. " enemyUnitID is " .. enemyUnitID .. " range is " .. MaxRange .. " in cellUnits is " .. range .. " with granularity_x " .. Map.granularity_x .. "\n")
	for i = 0,iterations + 1 do
		for j = 0,iterations  do 
			local tempIndex = firstNodeIndex + j + i *  Map.granularity_z
			if(ValidIndex( tempIndex,Map) == true)then
			
			
				local node = Grid[tempIndex]
				if(node == nil)then
					Spring.Echo("Error")
				end
				local UpdatedNode = node
				if( y - node.y < 500 )then
					UpdatedNode	 = NewNode ( node.index, CostIncrease ,node.parent , node.x, node.y , node.z )
				else
					local tmpCost = CostIncrease / 10
					
					UpdatedNode = NewNode ( node.index, tmpCost ,node.parent , node.x, node.y , node.z )
				end
				--myfile:write ("node index " .. node.index .. " is now dangerous with cost " .. CostIncrease .. "\n")
				Grid[tempIndex] = UpdatedNode
			end
		end
	end
	myfile:write("END\n")
end





local SpringGetUnitPosition = Spring.GetUnitPosition

return function(EnemyUnits , Map , CostIncrease )

	local myfileName = "dangZone.txt"
	myfile = io.open(myfileName, "w")

	local MaxRange = 0
	Grid = Map.Grid
	for index,enemyUnitID in pairs (EnemyUnits)do
		local thisUnitDefID = Spring.GetUnitDefID(enemyUnitID)
		if(thisUnitDefID ~= nil)then
			local weaponsTable  = UnitDefs[thisUnitDefID].weapons
			if (weaponsTable ~= nil)then
					for indexOfWeapons, weaponInfo in pairs (weaponsTable) do
					local thisWeaponDefID =  weaponInfo.weaponDef 
					if(thisWeaponDefID ~= nil )then
							local thisWeaponDef=WeaponDefs[thisWeaponDefID]					
						if(thisWeaponDef ~= nil )then
							local range = thisWeaponDef.range 
							if(range > MaxRange)then
								MaxRange = range 
							end
							
						end
					end
				end
			end
			
			UpdateMap(enemyUnitID,MaxRange,CostIncrease,Map)
			MaxRange = 0
		end
	end
	Map.Grid = Grid
	bb.Map = Map
	return Map
end
