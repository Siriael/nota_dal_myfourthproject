local sensorInfo = {
	name = "myPosition",
	desc = "Return position of the point unit.",
	author = "DarioLanza",
	date = "2017-04-22",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end


local function Swap(i,j,listOfUnits)

	local temp = listOfUnits[i]
	listOfUnits[i] = listOfUnits[j]
	listOfUnits[j] = temp

end


local SpringGetUnitPosition = Spring.GetUnitPosition
-- @argument unitID number
-- @description return static position of the first unit
return function(listOfUnits,point)
	
	local change = true
	
	while(change == true)do
		change = false
		for i= 1, #listOfUnits-1 do
			local pos_1 = Sensors.nota_dal_myfourthproject.myPosition(listOfUnits[i])
			dist_1 = pos_1:Distance(point)
			local pos_2 = Sensors.nota_dal_myfourthproject.myPosition(listOfUnits[i+1])
			dist_2 = pos_2:Distance(point)
				if ( dist_1 > dist_2)then
					Swap(i,i+1,listOfUnits)
					change = true
				end	
		end
	end
	
	
	
	
	
	
	return  listOfUnits
	--Spring.Echo(dist)
end