local sensorInfo = {
	name = "Form_NGroups",
	desc = "Split in N groups",
	author = "Dario Lanza",
	date = "2017-11-04",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end



--return function(AtlasUnits, UnitsToRescue )
return function ()
		--[[  
		TaskFields:
			Task.SavedUnits
			Task.DangerousUnits
			Task.DeadUnits
			Task.UnitsToRescue 
			report.GroupDead
		--]]
		local AtlasUnits = bb.AtlasUnits
		local UnitsToRescue = bb.UnitsToBeRescued
		
		local missionInfo = Sensors.core.MissionInfo()
		for indexAtlas,mAtlasID in pairs(AtlasUnits)do
			if(bb[mAtlasID] == nil and Spring.ValidUnitID(mAtlasID) )then
				for indexUnitToRescue,mUnitToRescueID in pairs (UnitsToRescue)do
					if (bb[mUnitToRescueID] == nil and Spring.ValidUnitID(mUnitToRescueID) )then
						local UnitToRescuePosition = Sensors.nota_dal_myfourthproject.myPosition(mUnitToRescueID)						
						if(UnitToRescuePosition:Distance(missionInfo.safeArea.center) > 600)then
							bb[mAtlasID] = mUnitToRescueID
							bb[mUnitToRescueID] = mAtlasID
							PairedUnits = { AtlasID = mAtlasID , UnitToRescueID = mUnitToRescueID }
							return PairedUnits 
						end
					end
				end
			end
		end
		
end
