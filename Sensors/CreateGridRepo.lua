--[[function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move custom group to defined position. Group is defined by table of unitID => formationIndex.",
		parameterDefs = {
			{ 
				name = "atlas",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			-- @parameter atlas [table] - mapping unitID => positionIndex
			{ 
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}

		}
	}
end]]


local sensorInfo = {
	name = "loadUnitsInHovercraft",
	desc = "Returns list of enemy units",
	author = "DarioLanza",
	date = "2010-04-24",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- cachining results for multiple calls in one AI frame

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description load units in an Area
-- @argument listOfUnits [array of unitIDs] unfiltered list 
-- @argument x number
-- @argument z number
-- @argument radius number



local function NewNode (index,cost,parent,pos_x,pos_y, pos_z)

	local node = { index = index,cost = cost,parent = parent , x = pos_x, y=pos_y, z = pos_z}


return node
end


local function PrintNode(resNode)
	Spring.Echo("index " , resNode.index ," position " , resNode.x ,  resNode.z , " cost " , resNode.cost , " parent " , resNode.parent)

end

local function AddToGrid(node)

	if(_Grid[node.index] ~= nil ) then
		Spring.Echo("Node already present from Grid")
		return false
	else
		_Grid[node.index] = node
		return true
	end
	
end

--************************************************     PriorityQueue     **************************************************************



return function(granularityX,granularityZ)
	
	
	mgranularity_x = Game.mapSizeX/granularityX
	mgranularity_z = Game.mapSizeZ/granularityZ
		
		local myfileName = "CreateGridOutput.txt"
	myfile = io.open(myfileName, "w")
	
	
	 _Grid = {}

	local i=0
	local x = 0
	local z = 0
	
	--+ mgranularity_z
	for x =  mgranularity_x,  Game.mapSizeX , mgranularity_x do
		for z =  mgranularity_z,  Game.mapSizeZ , mgranularity_z do	
			--i = z + x *granularityZ
			i = i+1
			y = Spring.GetGroundHeight(x, z)
			--Spring.Echo(x,z)
		--myfile:write(" x " .. x .. " z ".. z .. " index " .. i .. "\n")
			local node = NewNode(i,0,-1,x,y,z)
		--	PrintNode(node)
			AddToGrid(node)
			
		end
	end
	

	local myMap = { Grid = _Grid , granularity_x = granularityX , granularity_z = granularityZ }
	
	return 	myMap
	
end





