local sensorInfo = {
	name = "Form_NGroups",
	desc = "Split in N groups",
	author = "Dario Lanza",
	date = "2017-11-04",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- instant, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description Take passed positons, do some transformations over them and return the result back

local function Distance(node_1,node_2)
	dist = math.sqrt (  (node_1.x - node_2.x ) * ( node_1.x - node_2.x ) + ( node_1.z - node_2.z )*( node_1.z - node_2.z) )
	Spring.Echo("dist is " , dist)
return dist
end




-- @description return filtered list of listOfUnits
-- @argument numberOfGroups number 
-- @argument listOfUnits [array of unitIDs] unfiltered list 

-- @return groupArrays [table] filtered list
return function(Task )
	--local Task = { SavedUnits = {} , DeadUnits = {} , UnitsToRescue = groupArray , AliveGroups = mAliveGroups}
	local listOfUnits = Task.UnitsToRescue
	numberOfGroups = 4
	local numberOfUnits = 1
	for k = 1, #listOfUnits do
		tempListOfUnits = listOfUnits[k]
		for    index,UnitId in pairs(tempListOfUnits) do
				numberOfUnits = numberOfUnits + 1
		end
	end	
	
	
	
	
	local groupSize = math.floor(numberOfUnits / numberOfGroups)
	local groupArray = {}
	local missInfo = Sensors.core.MissionInfo()
	local found
	local tempGroup = {}
	local ListIsEMPTY = false
	local ListHasNoHead = false
	for k = 1, #listOfUnits do
		local tempListOfUnits = listOfUnits[k]
			if(tempListOfUnits[1] == nil)then 
				for    index,UnitId in pairs(tempListOfUnits) do
					tempListOfUnits[1] = UnitId
					tempListOfUnits[index] = nil
					break
				end
				if(tempListOfUnits[1] == nil)then 
					ListIsEMPTY = true
				end
				
				listOfUnits[k] = tempListOfUnits
			end	
	
	
		if(not ListIsEMPTY)then	
			for    index,UnitId in pairs(listOfUnits[k] ) do
				
				if(UnitId ~= nil)then
					local pos = Sensors.nota_dal_myfourthproject.myPosition(UnitId)
						
						if (pos:Distance(missInfo.safeArea.center)>missInfo.safeArea.radius) then
							found = false
									for index2,Temp_unitID in pairs (Task.DeadUnits)do
										if( Temp_unitID == unitIDs )then
											found = true
										end
									
									end
									if(found == false)then
										tempGroup[#tempGroup + 1 ] = UnitId
									end
						end
						
						if #tempGroup == groupSize then
							groupArray[#groupArray + 1 ] = tempGroup
							tempGroup = {}
							--#tempGroup = 0
						end
					
				end
				
				
			end
	
		
		end
	end
	if #tempGroup < groupSize then
				groupArray[#groupArray + 1 ] = tempGroup
		end
	Task.UnitsToRescue = groupArray
	
	return Task  
	
end