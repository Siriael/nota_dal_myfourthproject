local sensorInfo = {
	name = "pathFinder",
	desc = "CalculatePath",
	author = "Dario Lanza",
	date = "2017-11-04",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end


local function NewNode (index,cost,parent,pos_x,pos_y, pos_z)

	local node = { index = index,cost = cost,parent = parent , x = pos_x, y=pos_y, z = pos_z}


return node
end


local function PrintNode(resNode)
	Spring.Echo("index " , resNode.index ," position " , resNode.x ,  resNode.z , " cost " , resNode.cost , " parent " , resNode.parent)

end


--************************************************     PriorityQueue     **************************************************************


local function InitializePQ()
local _myTable = {}
local _minNode = 1
local _size = 0
 PQ = {myTable = _myTable,minNode = _minNode,size = _size}
 MAX_PRIORITY = 100000000
end

local function AddToPQ(node)
	
	if(PQ.myTable[node.index] ~= nil )then
		Spring.Echo("ERROR,node already present from PQ")
		return nil
	else
		PQ.myTable[node.index] = node
		PQ.size = PQ.size+1
	end
	
	--Spring.Echo("Inside AddToPQ")
	if(PQ.size == 1) then
	--Spring.Echo("SpecialCase")
		PQ.minNode =  node.index
	else
	
	
		if( node.cost < PQ.myTable[PQ.minNode].cost ) then
			PQ.minNode =  node.index
		end
	end
	--Spring.Echo("Outside AddToPQ")
end


local function ExtractMinFromPQ ()
	
	
	local node = PQ.myTable[PQ.minNode]
	PQ.myTable[PQ.minNode] = nil
	
	local min_prio = MAX_PRIORITY
	local min_index = 1
	
	for index, PQ_node in pairs(PQ.myTable) do

			if( PQ_node.cost <= min_prio )then
			min_prio = PQ_node.cost
			min_index = index
			end
			
	end
	PQ.minNode = min_index
	PQ.size = PQ.size-1
	return node
	
end

local function RemoveFromPQ(index)

	if(PQ[index] == nil ) then
		Spring.Echo("Node NOT present from PQ")
		return false
	else
		PQ[index] = nil
		return true 
		
	end

end

--************************************************     VisitedSet     **************************************************************


local function AddToVisitedSet(node)

	if(VisitedSet[node.index] ~= nil ) then
	--	Spring.Echo("Node already present from VisitedSet")
		return false
	else
		VisitedSet[node.index] = node
		return true
	end
	
end

local function GetFromVisitedSet(index)

	if(VisitedSet[index] == nil ) then
		Spring.Echo("Node NOT present from VisitedSet")
		return nil
	else
		return VisitedSet[index] 
		
	end
	
end





local function CheckIf_VisitedSet_ContainsNode(index)
	if(VisitedSet[index] == nil ) then
		--Spring.Echo("Node NOT present from VisitedSet")
		return false
	else
		return true
		
	end


end







--************************************************     FrontierMap     **************************************************************





local function AddToFrontierMap(node)
	
	FrontierMap[node.index] = node
	return 
	end
	--[[if(FrontierMap[node.index] ~= nil ) then
	--	Spring.Echo("Node already present from FrontierMap")
		return false
	else
		FrontierMap[node.index] = node
		return true
	end
	--]]
--end

local function GetFromFrontierMap(node)

	if(FrontierMap[node.index] == nil ) then
	--	Spring.Echo("Node NOT present from FrontierMap")
		return nil
	else
		return FrontierMap[node.index] 
		
	end
	
end





local function CheckIf_FrontierMap_ContainsNode(node)
	if(FrontierMap[node.index] == nil ) then
		--Spring.Echo("Node NOT present from FrontierMap")
		return false
	else
		return true
		
	end


end
--


--************************************************    Grid   **************************************************************

local function AddToGrid(node)

	if(Grid[node.index] ~= nil ) then
		--Spring.Echo("Node already present from Grid")
		return false
	else
		Grid[node.index] = node
		return true
	end
	
end

local function GetFromGrid(index)

	if(Grid[index] == nil ) then
		Spring.Echo("Node NOT present")
		return nil
	else
		return Grid[index] 
		
	end
	
end


--************************************************   UtilityFunction  **************************************************************

local function Distance(node_1,node_2)
	dist = math.sqrt (  (node_1.x - node_2.x ) * ( node_1.x - node_2.x ) + ( node_1.z - node_2.z )*( node_1.z - node_2.z) )
	--Spring.Echo(dist)
return dist
end

local function GetCost(index)
	local tmpNode = GetFromGrid(index)
	local ActualCost = 100
	--myfile:write ("cost is "tmpNode.y)
	if(tmpNode.cost >= 100)then
		--myfile:write("Node with index " .. tmpNode.index .. " has a cost of " .. tmpNode.cost .. "\n")
	end	
	if(tmpNode.y <= 0)then
		tmpNode.y = 10
	end
	
	if(tmpNode.y>= 0 and tmpNode.y <= 350)then
		ActualCost = 2
	else
		ActualCost = 300
	end
	local EstimatedCost = Distance(tmpNode , GoalNode)
	local res = EstimatedCost + ActualCost + tmpNode.cost
	--myfile:write("Node with index " .. tmpNode.index .. " has a cost of " .. tmpNode.cost .. "\n")
		return 	EstimatedCost + ActualCost + tmpNode.cost
	end


local function CheckThatDoesntLeadToParentNode(node,indexOfChild)

	if(node.parent == indexOfChild)then
		return true
	else
		return false
	end
end








local function GetNeighbourns(index)
	local Neighbourns = {}
	local Right = false
	local Left = false
	local Up = false
	local Bottom = false
	--Spring.Echo("Max_X" , Max_X , "Max_Z " , Max_Z)
	if( index % Max_X ~= 0) then
		Right = true
	--	myfile:write("SUCCESS on R \n")
	---	Spring.Echo("SUCCESS on R")
		--PrintNode(Grid[index + 1])
		Neighbourns[#Neighbourns+1] = Grid[index+1]
	end
	if(index % Max_X ~= 1 ) then
	Left= true
		--myfile:write("SUCCESS on L \n")
		--Spring.Echo("SUCCESS on L")
		--PrintNode(Grid[index - 1])
		Neighbourns[#Neighbourns+1] = Grid[index-1]
	end
	if(index - Max_X >= 1) then
	Up = true
		--myfile:write("SUCCESS on U \n")
	--Spring.Echo("SUCCESS on U")
	--PrintNode(Grid[index - Max_X])
		Neighbourns[#Neighbourns+1] = Grid[index - Max_X]
	end
	if(index + Max_X <= Max_X*Max_Z) then
	Bottom = true
		--Spring.Echo("SUCCESS on B")
	--	myfile:write("SUCCESS on B \n")
		--PrintNode(Grid[index + Max_X])
		Neighbourns[#Neighbourns+1] = Grid[index + Max_X]
		--PrintNode(Grid[index + Max_X])
	end
	
	if(Right and Up)then
		Neighbourns[#Neighbourns+1] = Grid[ index+1 - Max_X]
	end
	
	if(Right and Bottom)then
		Neighbourns[#Neighbourns+1] = Grid[ index+1 + Max_X]
	end
	
	if(Left and Up)then
		Neighbourns[#Neighbourns+1] = Grid[ index - 1 - Max_X]
	end
	
	if(Left and Bottom)then
		Neighbourns[#Neighbourns+1] = Grid[ index - 1 + Max_X]
	end
	
	return Neighbourns
end




--[[
local  function AddToPQ(node)

	if( PQ.myTable[node.index] ~= nil ) then
		Spring.Echo("Node already present")
		return false
	else
		PQ.myTable[node.index] = node
		return true
	end
	
end
--]]


--[[
local function TestFrontierMap()
	granularity = 100
	local i=1
	FrontierMap = {}
	
	Spring.Echo("mapSizeX " , Game.mapSizeX)
	for x = 0,  Game.mapSizeX, granularity do
		Spring.Echo("hey x")
		for z = 0,  Game.mapSizeZ, granularity do
		Spring.Echo("hey z")
			y = Spring.GetGroundHeight(x, z)
			i = i+1
			local node = NewNode(i,0,nil,x,y,z)
			
			AddToFrontierMap(node)
		end
	end
	Spring.Echo("Let's add  a node that should be already present")
	local tempnode = NewNode(2,0,nil,20,40,50)
	AddToFrontierMap (tempnode)
	local resNode = GetFromFrontierMap (tempnode)
	PrintNode(resNode)

end





local function TestPQ()
	local i = 1
	for x = 0,  Game.mapSizeX, granularity_x do
	
		for z = 0,  Game.mapSizeZ, granularity_z do	
			y = Spring.GetGroundHeight(x, z)
			local node = NewNode(i,i,nil,x,y,z)
		--	PrintNode(node)
			AddToPQ(node)
			i = i+1
		end
	end
	
	
	local minNode = ExtractMinFromPQ()
	PrintNode(minNode)
	minNode = ExtractMinFromPQ()
	PrintNode(minNode)
	local node = NewNode(1,1,nil,0,3,4)
	Spring.Echo("add a new node")
	AddToPQ(node)
	minNode = ExtractMinFromPQ()
	PrintNode(minNode)
	minNode = ExtractMinFromPQ()
	PrintNode(minNode)
	
	
	
end


local function TestGrid()
	
	local DearNeighbourns = {}
	
	DearNeighbourns = GetNeighbourns(100)
	Spring.Echo("TestGrid")

	for index, node in pairs(DearNeighbourns) do
			PrintNode (node)
		end
			
	--[[for i = 1, #DearNeighbourns  do 
	
		PrintNode (DearNeighbourns[i])
	i=i+1
	end--]]
	Spring.Echo("EndTest")
--end

local function TestUtilFunction()

	local actualNode = GetFromGrid(1)
	AddToVisitedSet(actualNode)
	local neig = GetNeighbourns(actualNode.index)
	for index,node in pairs(neig) do
		AddToVisitedSet(NewNode(node.index,12,actualNode.index,10,20,30))
	end
	actualNode = GetFromVisitedSet(2)
	neig = GetNeighbourns(actualNode.index)
	for index,node in pairs(neig) do
		local res = CheckThatDoesntLeadToParentNode(actualNode,index)
		
		--	Spring.Echo(res)
		
	end
	
end

local function Initialize ()

	--local fileName = "LuaUI/export/capture" .. "-" .. Game.mapName .. "-line-" .. z+1 .. "-column-" .. x+1 .. ".lua" --txt
	local myfileName = "PathFinderLog.txt"
	myfile = io.open(myfileName, "w")
		--	myfile:write("---------------------------------------------------------------------")
		
			--MakeOneTile(file, x*squareSize, (x+1)*squareSize - 1, z*squareSize, (z+1)*squareSize - 1, step, squareSize, polishedBorderPadding)
			
		
			

end
--]]

local function BFS ()

local Queue = {}
 visited = {}
--local listOfNodes = GetNeighbourns(StartNode.index)
Queue[#Queue + 1 ] = StartNode
local Marked = {}
local size = 1
while (size > 0)do
	
	local ActualNode = Queue[#Queue]
	Queue[#Queue] = nil
	size = size -1 
	if(ActualNode.index == GoalNode.index)then
		return ActualNode
	end
	
		--Spring.Echo("PQ entered, size is  ".. size .."\n")
	local listOfNodes = GetNeighbourns(ActualNode.index)
	for index,node in pairs (listOfNodes) do
		if (Marked[node.index] ~= 1 )then
		
			if(node.cost<=1000)then
				local tempnode = NewNode(node.index,node.cost,ActualNode.index,node.x,node.y,node,z)
				visited[tempnode.index] = tempnode
				Queue[#Queue+1] = tempnode
				size = size + 1 
			end
		end
	end
	Marked[ActualNode.index] = 1
end

return nil


end

local function PathFinder()
	
--	StartNode = GetFromGrid(1)
		AddToPQ(StartNode)
--While havent checked all the nodes in the frontier 
		while(PQ.size  ~= 0 ) do 
			local skip = false
		--  local actualNode = PQ.GetNextNode()
			local actualNode = ExtractMinFromPQ()
			--myfile:write ("index " .. actualNode.index , " cost " , actualNode.cost , " vs ", GoalNode.index .. "\n")
			
		--	Spring.Echo (actualNode.index , "cost" , actualNode.cost , " vs ", GoalNode.index)
			if (actualNode.index == GoalNode.index) then
				return actualNode;
			end
			if(CheckIf_VisitedSet_ContainsNode(actualNode) == true) then
				skip = true;
			--	myfile:write("Skip \n")
			--	Spring.Echo("Skip")
			else
				--VisitedSet[actualNode.index] = ActualNode
				AddToVisitedSet(actualNode)
			end
			if(skip == false )then
				
				local listOfNodes = GetNeighbourns(actualNode.index)
			--	Spring.Echo("num of Nodes opened" , #listOfNodes)
				for index,node in pairs(listOfNodes )do
					local skip2 = false
				--	PrintNode(node)
					if(CheckThatDoesntLeadToParentNode(actualNode,node.index)) then
					--	myfile:write(" node with index " .. node.index .. " lead to parent with index " ..actualNode.index .. "\n")
						--Spring.Echo("lead to parent")				
						skip2 = true 
					end
					
					
					if skip2 == false then
						local tmpcost = GetCost(node.index)
				--		local tempnode =  GetFromGrid (node.index)
						
						local childNode = NewNode( node.index , tmpcost , actualNode.index , node.x , node.y , node.z)
					--	PrintNode(childNode)						 
						if(FrontierMap[childNode.index]  ~= nil)then
						--Spring.Echo("New possible path found")
							oldchild = FrontierMap[childNode.index]
							--GetFromFrontierMap(childNode)
							if( childNode.cost < oldchild.cost  ) then
							--	Spring.Echo("New path found")
								RemoveFromPQ(oldchild.index)
								AddToPQ(childNode)
							end
						else
							AddToPQ(childNode)
							FrontierMap[childNode.index] = childNode
							--AddToFrontierMap(childNode)
						end
						
						
					end
				end
			end
			
		end

		return nil
end


local function GetTrueCoordinates(index)
	local node = Grid[index]
	if(node == nil)then
		--Spring.Echo("error is here")
	end
	--myfile:write("index is " .. node.index .. "position is x " .. node.x .. " z " .. node.z .. " \n")
	return  Vec3(node.x,node.y,node.z)
	
	
end

local function ReversePath(myPath)
	local iterations = #myPath
	local reversedPath = {}
	for index,localnode in pairs(myPath)do
		reversedPath[iterations] = localnode
		iterations = iterations - 1
	end
	reversedPath[1] = StartNode
	return reversedPath
end

local function ClearTables()
	Grid = nil
	VisitedSet   = nil
	FrontierMap  = nil
	PQ = nil
end

return function (StartNodeIndex,GoalNodeIndex,atlasID,Map)
	
	Initialize()
	--[[local Map = bb.Map
	local StartNodeIndex = parameter.StartIndex
	local GoalNodeIndex = parameter.GoalIndex
	
	local atlasGroup = parameter.atlas
	--Spring.Echo(atlasGroup)
	local atlasID = atlasGroup
	--]]
	
	
	granularity_x = Game.mapSizeX/Map.granularity_x
	granularity_z = Game.mapSizeZ/Map.granularity_z
	if( GoalNodeIndex > (Map.granularity_x - 1) * (Map.granularity_z - 1) )then
	--	myfile:write("Failure on " .. GoalNodeIndex .. " GoalNodeIndex \n")
		GoalNodeIndex = GoalNodeIndex - Map.granularity_x
		--return FAILURE
	end
	if( StartNodeIndex > (Map.granularity_x - 1) * (Map.granularity_z - 1) )then
	--	myfile:write("Failure on " .. StartNodeIndex .. " StartNodeIndex \n")
		StartNodeIndex = StartNodeIndex - Map.granularity_x
		--return FAILURE
	end
	
	
	if( StartNodeIndex == 0  )then
		StartNodeIndex = StartNodeIndex + 1
	end
	if( StartNodeIndex < 0  )then
	--	myfile:write("Failure on " .. StartNodeIndex .. " StartNodeIndex \n")
		StartNodeIndex = StartNodeIndex + Map.granularity_x
		--return FAILURE
	end
	
	if( GoalNodeIndex < 0  )then
		--myfile:write("Failure on " .. GoalNodeIndex .. " GoalNodeIndex\n")
		GoalNodeIndex = GoalNodeIndex + Map.granularity_x
		--return FAILURE
	end
	
	Grid = Map.Grid
	GoalNode  = GetFromGrid(GoalNodeIndex)
	StartNode  = GetFromGrid(StartNodeIndex)
	if(GoalNode == nil)then
		--myfile:write("Failure on " .. GoalNodeIndex .. " GoalNodeIndex \n")
		--Spring.Echo ("GoalNodeIndex is " .. GoalNodeIndex)
		return nil
	end
	VisitedSet = {}
	FrontierMap = {}
	InitializePQ()
	
	

	finalpath = nil
	Max_X = Map.granularity_x
	Max_Z = Map.granularity_z
	
	local res = PathFinder()
	--local res = BFS()
	--local res = StartNode
	--Spring.Echo("StartNodeIndex " .. StartNodeIndex .. " GoalNodeIndex " .. GoalNodeIndex)
	if(res == nil) then
		
		Spring.Echo ("failure")
	return nil
	else
		local myPath = {}
		local tempNode_1 = res
		
		--Spring.Echo(tempNode_1.parent)
		--myfile:write("PATH \n")	
		while(tempNode_1.parent ~= -1 )do
			myPath[#myPath + 1 ] = tempNode_1
			tempNode_1 = VisitedSet [tempNode_1.parent]
--tempNode_1 = visited [tempNode_1.parent]
			--myfile:write("index is " .. tempNode_1.index .. " \n")
		end
		myPath[#myPath + 1 ] = GoalNode
		local tempPath = ReversePath( myPath)
		--local tempPath = myPath
		--Spring.Echo("index is ", tempPath[1].index)
		--PrintNode(Grid[tempPath[1].index])
		--return tempPath
		local finalpath = {}
		for index, node in pairs(tempPath)do
			finalpath[#finalpath+1] = GetTrueCoordinates(tempPath[index].index)
		end
		
		myfile:write("END\n")
		io.close()
		local myTable = {}
			
		myTable = { PathIndex = 1 , UnitStucked = 0 ,StuckedTime = 0 , OrderGiven = false , path = finalpath}
		--bb[atlasID] = myTable
		
		return myTable
		
	end
	
	ClearTables()
	end







	

