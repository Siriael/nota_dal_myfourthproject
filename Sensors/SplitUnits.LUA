local sensorInfo = {
	name = "SplitUnits",
	desc = "Filter list of units by category.",
	author = "PepeAmpere",
	date = "2017-05-30",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- no caching 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description return filtered list of units
-- @argument listOfUnits [array of unitIDs] unfiltered list 
-- @argument category [table] BETS category
-- @return newListOfUnits [array of unitIDs] filtered list
return function(listOfUnits, category)
	local TableReturned
	local newListOfUnits = {}
	local Atlas = {}
	local Scouts = {}
	local UnitsToBeSaved = {}
	local Miscellaneous = {}
	for i=1, #listOfUnits do
		local thisUnitID = listOfUnits[i]
		local thisUnitDefID = Spring.GetUnitDefID(thisUnitID)
		if ( isTransport(thisUnitID)  ~= nil) then -- in category
			Atlas[#Atlas+ 1] = thisUnitID
		
		else
			 if( isGroundUnit(thisUnitDefID) == true ) then
			 UnitsToBeSaved[#UnitsToBeSaved + 1] = thisUnitDefID
			 else
				Miscellaneous[#Miscellaneous + 1 ] = thisUnitDefID
			end
		end
	end
	
	local groupArray = {Atlas, Scouts, UnitsToBeSaved,Miscellaneous}
	
	return groupArray
end