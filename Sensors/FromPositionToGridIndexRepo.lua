--[[function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move custom group to defined position. Group is defined by table of unitID => formationIndex.",
		parameterDefs = {
			{ 
				name = "atlas",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			-- @parameter atlas [table] - mapping unitID => positionIndex
			{ 
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}

		}
	}
end]]


local sensorInfo = {
	name = "loadUnitsInHovercraft",
	desc = "Returns list of enemy units",
	author = "DarioLanza",
	date = "2010-04-24",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- cachining results for multiple calls in one AI frame

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description load units in an Area
-- @argument listOfUnits [array of unitIDs] unfiltered list 
-- @argument x number
-- @argument z number
-- @argument radius number





local function GetTrueCoordinates(index)
	local node = Map.Grid[index]
	if(node == nil)then
	Spring.Echo("error is here")
	end
	return  Vec3( node.x + granularity_x/2,node.y,node.z + granularity_z/2 ) 
	
	
end


return function(pos,Map)
	--local Max_x = Game.mapSizeX/ Map.granularity_x
	
	
	local tmpx = Game.mapSizeX/Map.granularity_x
	local tmpz = Game.mapSizeZ/Map.granularity_z
	local x = pos.x/tmpx
	local z = pos.z/tmpz
	local myfileName = "test.txt"
	myfile = io.open(myfileName, "w")
	local i = math.ceil(x)
	local j = math.ceil(z)
	myfile:write (" x " .. x ..  " z " .. z .. " tmpx " ..tmpx .. " tmpz " .. tmpz .. " pos.x ".. pos.x .. "pos.z" .. pos.z   )
	myfile:write (" i ".. i .. " j " .. j )
	--index = j + i  * Map.granularity_x 
	
	-- WORKING FORMULA (provided the grid is set-up as before)local index = (j)+ (i ) * Map.granularity_z 
	local index = j + i  * Map.granularity_z 
	--io.close
	return index
	
end





