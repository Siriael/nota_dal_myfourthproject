local sensorInfo = {
	name = "Form_NGroups",
	desc = "Split in N groups",
	author = "Dario Lanza",
	date = "2017-11-04",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- instant, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description Take passed positons, do some transformations over them and return the result back

local function Distance(node_1,node_2)
	dist = math.sqrt (  (node_1.x - node_2.x ) * ( node_1.x - node_2.x ) + ( node_1.z - node_2.z )*( node_1.z - node_2.z) )
	Spring.Echo("dist is " , dist)
return dist
end



-- @description return filtered list of listOfUnits
-- @argument numberOfGroups number 
-- @argument listOfUnits [array of unitIDs] unfiltered list 

-- @return groupArrays [table] filtered list
return function(Task,UnitsToRescue)
	
	numberOfGroups = 4 
	local templist = {}
	local i = 1
	local missInfo = Sensors.core.MissionInfo()
	local groupArray = {}
	for index,UnitId in pairs (UnitsToRescue) do
	
		if(Spring.ValidUnitID(UnitId))then
			local pos = Sensors.nota_dal_myfourthproject.myPosition(UnitId)
				if (pos:Distance(missInfo.safeArea.center)>missInfo.safeArea.radius) then
					
					
					
					
					if(i>4)then
						i = 1
					else
						i= i +1
					end
					
					
					local temp = groupArray[i]
					if(temp == nil)then
						temp = {}
						temp[1] = UnitId
					else
						temp[#temp + 1] = UnitId
					end
					
					groupArray[i] = temp
				end
		end
		
		
		
	end
	Task.UnitsToRescue = groupArray
	return Task
end
	
	
