local sensorInfo = {
	name = "Form_NGroups",
	desc = "Split in N groups",
	author = "Dario Lanza",
	date = "2017-11-04",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description Take passed positons, do some transformations over them and return the result back





-- @description return filtered list of listOfUnits
-- @argument numberOfGroups number 
-- @argument listOfUnits [array of unitID] unfiltered list 

-- @return groupArrays [table] filtered list

return function(Task , report )
		--[[  
		TaskFields:
			Task.SavedUnits
			Task.DangerousUnits
			Task.DeadUnits
			Task.UnitsToRescue 
			report.GroupDead
		--]]
		for index_group,GroupID in pairs (Task.UnitsToRescue) do
			local tempTable = GroupID
			for index,UnitID in pairs (tempTable) do
				if UnitID == report.listOfUnits[1] then			
				
					if(report.result == false)then
						if (report.GroupDead == true) then
							Spring.Echo ("UNIT IS DEAD")
							Task.DangerousUnits[#Task.DangerousUnits+1] = unitID
							tempTable[#tempTable+1] = tempTable[index]
						else
							if(not Spring.ValidUnitID(unitID))then
								Task.DeadUnits[#Task.DeadUnits + 1] = unitID
							end
								tempTable[#tempTable+1] = tempTable[index]
								--Task.UnitsToRescue[tempTable][#UnitID +1 ] = Task.UnitsToRescue[tempTable][UnitID] 
								--Task[#Task + 1 ] = Task[index]
						end
					else
						Task.SavedUnits[#Task.SavedUnits+1] = unitID
					end 
					Spring.Echo("tempTable",tempTable[index])
					tempTable[index] = nil
				end
			end
			
			Task.UnitsToRescue[index_group] = tempTable
		end
	
	
	return Task
end
