local sensorInfo = {
	name = "Form_NGroups",
	desc = "Split in N groups",
	author = "Dario Lanza",
	date = "2017-11-04",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description Take passed positons, do some transformations over them and return the result back

local function ReversePath(myPath)
	local iterations = #myPath
	local reversedPath = {}
	for index = 0, #myPath do 
		reversedPath[iterations] = myPath[index]
		iterations = iterations - 1
	end
	reversedPath[1] = myPath[#myPath]
	return reversedPath
end

return function (AtlasID,path)
	
	
	local myTable = path
	myTable.PathIndex = 0
	
	local path =  myTable.path
	local reversedPath = ReversePath(path)
	myTable.path = reversedPath
	return myTable
	
	
	
end





