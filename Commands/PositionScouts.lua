function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move custom group to defined position. Group is defined by table of unitID => formationIndex.",
		parameterDefs = {
			{ 
				name = "peepers",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			-- @parameter atlas [table] - mapping unitID => positionIndex
			{ 
				name = "map",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}

		}
	}
end

local function ClearTable()
	local mytmpTable = bb[atlasID]
	mytmpTable.PathIndex = 1
	mytmpTable.UnitStucked = 0
	mytmpTable.StuckedTime = 0
	mytmpTable.OrderGiven = nil
	mytmpTable.path = nil
	bb[self.atlasID] = mytmpTable
	mytmpTable= nil

end

local function ClearState(self)
	
	mpath = nil
	atlasGroup = nil
	atlasID = nil
	pointX = nil
	pointY = nil
	pointZ = nil
	atlasPosition = nil
	dest = nil
	
end

local threshold =  300

local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

function Run(self,units,parameter)
	
	
	local running = true
	local peepers = parameter.peepers
	local map = parameter.map
	local step =  Game.mapSizeX / #peepers
	local mPassedFromStart = {}
	local mStillRunning = {}
	local mOrderGiven = {}
	local cmdID = CMD.MOVE
	if (bb.ScoutState == nil)then
		for i = 0 , #peepers do
			mStillRunning[#mStillRunning + 1 ] = false
			mOrderGiven[#mOrderGiven + 1 ] = false
			mPassedFromStart[#mPassedFromStart+ 1 ] = false
		end
	else
		ScoutState = bb.ScoutState
		mPassedFromStart = ScoutState.PassedFromStart
		mStillRunning = ScoutState.StillRunning
		mOrderGiven = ScoutState.OrderGiven
	end
	local treshold = 950
	local myfileName = "ScoutReport.txt"
	myfile = io.open(myfileName, "w")
	--Initialize
	--for i = step , Game.mapSizeX , step do
	--	for index,UnitID in pairs (peepers)do
	for i = 0,#peepers do 
	
		local UnitID = peepers[i]   
		if(Spring.ValidUnitID(peepers[i])  )then
			
			if(mPassedFromStart[i] == nil)then
				mPassedFromStart[i] = false
			end
			
			local start = Vec3(i*step,100,100)
			local dest = Vec3(i*step,100,Game.mapSizeZ)
			
			local pointX, pointY, pointZ = SpringGetUnitPosition(UnitID)
			local peeperPosition = Vec3 ( pointX, pointY, pointZ)
			local dist = peeperPosition:Distance( start )
			myfile:write(" peeperPosition.Z " .. peeperPosition.z  .. " dest.Z " .. dest.z .. " dist " .. dist .. "\n")
			if( peeperPosition:Distance( start ) > treshold ) then
				if(not mOrderGiven[i] ) then
					SpringGiveOrderToUnit(UnitID, cmdID, start:AsSpringVector(), {'shift'})
					mOrderGiven[i] = true
				end
				if(mPassedFromStart[i])then
					mStillRunning[i] = false
				else
					mStillRunning[i] = true
				end
				
			else
				mPassedFromStart[i] = true
				mStillRunning[i] = false
			end
			
	
		else
			
			mStillRunning[i] = false
		end	
		
		
	end	
	
		
	
	local IsScoutMapRunning = false
	
	
	for i = 0,#peepers do
		local rez = mStillRunning[i]
		
		if(mStillRunning[i]) then
			IsScoutMapRunning = true
		end	
	end
	local ScoutState = { StillRunning = mStillRunning, PassedFromStart = mPassedFromStart, OrderGiven = mOrderGiven}
	bb.ScoutState = ScoutState
	bb.StillRunning = mStillRunning
	if(IsScoutMapRunning)then
		return RUNNING
	else
		return SUCCESS
	end
end


function Reset(self)
	ClearState(self)
end



