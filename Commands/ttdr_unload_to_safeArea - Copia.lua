function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move custom group to defined position. Group is defined by table of unitID => formationIndex.",
		parameterDefs = {
			{ 
				name = "saveAreaCenter",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
            },
            { 
				name = "saveAreaRadius",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
		}
	}
end


-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 0

-- speed-ups
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitPosition = Spring.GetUnitPosition

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
	self.lastPointmanPosition = Vec3(0,0,0)
end


function Run(self, units, parameter)
    local center = parameter.saveAreaCenter 
    local radius = parameter.saveAreaRadius
    local atlasUnits = {}


    local cmdID = CMD.UNLOAD_UNITS

    for i=1, #listOfUnits do
		local thisUnitID = listOfUnits[i]
		local thisUnitDefID = Spring.GetUnitDefID(thisUnitID)
		
		if (UnitDefs[thisUnitDefID].name == "armatlas") then
            atlasUnits[#atlasUnits + 1] = listOfUnits[i]
		end
    end
    
    
    for i=1, #atlasUnits do
        local posX, posY, posZ = SpringGetUnitPosition(center)
        SpringGiveOrderToUnit(atlasUnits[i], cmdID, {posX, posY, posZ, radius},  {}) --- load units into atlas 
    end 
    

    return RUNNING

end