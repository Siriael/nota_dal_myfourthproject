function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move custom group to defined position. Group is defined by table of unitID => formationIndex.",
		parameterDefs = {
 	{ 
				name = "atlas",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			-- @parameter atlas [table] - mapping unitID => positionIndex
			{ 
				name = "listOfRescueUnits",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
			
		}
	}
end


-- speed-ups
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitPosition = Spring.GetUnitPosition


function Run(self, units, parameter)
 	local atlasGroup = parameter.atlas
	local listOfUnitsToRescue = parameter.listOfRescueUnits 
	--local WantedPosition = parameter.position
	local thisUnitID
    local cmdID = CMD.LOAD_UNITS
	local radius = 300
		for posIndex, unitID in pairs(atlasGroup) do
		thisUnitID  = unitID		
		end
		Spring.Echo (listOfUnitsToRescue)
		
		
		local posX, posY, posZ = SpringGetUnitPosition(listOfUnitsToRescue[1])
	
	    --local posX, posY, posZ = SpringGetUnitPosition(listOfUnitsToRescue[1])
        --SpringGiveOrderToUnit(thisUnitID,    cmdID, {posX, posY, posZ, 0},  {}) --- load units into atlas 
		
	--	SpringGiveOrderToUnit(atlasUnits[i], cmdID, {posX, posY, posZ, 0},  {})
	--[[if (#Spring.GetUnitIsTransporting(thisUnitID) ~= 1) then
		return RUNNING
	else 
		return SUCCESS
	end
	--]]
	
	
		if (#Spring.GetUnitIsTransporting(thisUnitID) >= 1) then
			Spring.Echo(#Spring.GetUnitIsTransporting(thisUnitID))
			return SUCCESS
		else
			if(Spring.ValidUnitID(thisUnitID))then		
				SpringGiveOrderToUnit(thisUnitID, cmdID, {posX,posY,posZ, radius},  {}) --- load units into bear 
				Spring.Echo(#Spring.GetUnitIsTransporting(thisUnitID))
				return RUNNING
			else
				return FAILURE
			end
		end
	

    

end