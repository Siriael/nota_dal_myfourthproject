function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move custom group to defined position. Group is defined by table of unitID => formationIndex.",
		parameterDefs = {
			{ 
				name = "atlas",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			-- @parameter atlas [table] - mapping unitID => positionIndex
			{ 
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}

		}
	}
end


local function ClearState(self)
	--self.threshold = THRESHOLD_DEFAULT
	self.pointmanPosition = Vec3(0,0,0)
end

local threshold =  300

local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

function Run(self,units,parameter)
	
	local atlasGroup = parameter.atlas
	
	local WantedPosition = parameter.position
	local minIndex
	local pointmanID
	
	for posIndex,unitID in pairs(atlasGroup) do
		minIndex = posIndex 
		pointmanID = unitID		
	end
	
	--local myUnitID= atlas[1]
	
	
	local cmdID = CMD.MOVE
	
	local pointX, pointY, pointZ = SpringGetUnitPosition(pointmanID)
	local pointmanPosition = Vec3(pointX, pointY, pointZ)
	SpringGiveOrderToUnit(pointmanID, cmdID, WantedPosition:AsSpringVector(), {})
	if (pointmanPosition:Distance(WantedPosition) < threshold) then
		return SUCCESS
	else
		if(Spring.ValidUnitID(pointmanID))then
			
			return RUNNING								
		else
			return FAILURE
		end
		--Spring.Echo(pointmanPosition:Distance(WantedPosition))
		--SpringGiveOrderToUnit(myUnitID[1], cmdID, WantedPosition:AsSpringVector(), {})
		
		
	end
end

function Reset(self)
	ClearState(self)
end



